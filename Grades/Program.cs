﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace Grades
{
    class Program
    {
        static void Main(string[] args)
        {
            IGradeTracker book = CreateGradeBook();

            GetBookName(book);
            AddGrades(book);
            WriteResult(book);
        }

        private static IGradeTracker CreateGradeBook()
        {
            SpeechSynthesizer synth = new SpeechSynthesizer();
            synth.Speak("Hello! This is the grade book program!");

            return new ThrowAwayGradeBook();
        }

        private static void WriteResult(IGradeTracker book)
        {
            /*foreach (float grade in book)
            {
                Console.WriteLine(grade);
            }*/
            GradeStatistics stats = book.ComputeStatistics();
            WriteResult("The average grade is", stats.AverageGrade);
            WriteResult("The highest grade is", stats.HighestGrade);
            WriteResult("The lowest grade is", stats.LowestGrade);
            WriteResult(stats.Description, stats.LetterGrade);
        }

        public static void AddGrades(IGradeTracker book)
        {
            book.AddGrade(91);
            book.AddGrade(89.5f);
            book.AddGrade(75);
            book.WriteGrades(Console.Out);
        }

        private static void GetBookName(IGradeTracker book)
        {
            //book.NameChanged += OnNameChanged;
            try
            {
                Console.WriteLine("Please enter your name");
                book.Name = Console.ReadLine();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void WriteResult(string description, float result)
        {
            Console.WriteLine($"{description}: {result:F2}");
        }
        static void WriteResult(string description, string result)
        {
            Console.WriteLine($"{description}: {result:F2}");
        }

        static void OnNameChanged(object sender, NameChangedEventArgs args)
        {
            Console.WriteLine($"Grade book changing name from {args.ExistingName} to {args.NewName}");
        }
        /*using(StreamWriter outputFile = File.CreateText("Grades.txt"))
           {
               book.WriteGrades(outputFile);
               outputFile.Close();
           }*/
    }
}
